<link rel="stylesheet" type="text/css" href="assets/css/overwrite.css">
<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="https://bulma.io">
      <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="<?= $home; ?>">
        Home
      </a>

      <a class="navbar-item" href="https://bulma.io/documentation" target="_blank">
       Bulma Documentation
      </a>
			
			<a class="navbar-item" href="tasks" target="_blank">
        Tasks
      </a>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Button</strong>
          </a>
          <a class="button is-light">
            Button 2
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>