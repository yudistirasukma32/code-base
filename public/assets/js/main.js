//SLICK JS
$(document).ready(function(){
	if ( $('.single-item').length > 0 ) {
		$('.single-item').slick();
	}
});


//Responsive Slides
$(document).ready(function(){
if ( $('.rslides').length > 0 ) {
	$('.rslides').responsiveSlides({
			auto: false,
			speed: 500,
			timeout: 10000,
			nav: false,
			pager: true,
			manualControls: '#nav',
	});
}
});