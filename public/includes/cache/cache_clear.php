<?php
	// cache starts
	use phpFastCache\CacheManager;
	use phpFastCache\Core\phpFastCache;

	// In your class, function, you can call the Cache
	$InstanceCache = CacheManager::getInstance('predis');
	$InstanceCache->clear();
?>