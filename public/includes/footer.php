<html>
<link rel="stylesheet" type="text/css" href="assets/css/overwrite.css">
<body>
<footer class="footer">
	<div class="container">
		<div class="columns is-multiline is-mobile">
			<div class="column is-half-mobile is-10-tablet"><br/><p><a class="" href="mailto:<?=$setting->getEmail();?>"><?=$setting->getEmail();?></a></p><br/><small class="is-white-text">site by <a href="http://mydevteam.id" target="_blank" class="is-yellow-text bordered">MyDevTeam</a></small></div>
			<div class="column is-half-mobile is-2-tablet right"><br/><br/><small class="is-white-text">&copy; <?= date('Y'); ?></small></div>
		</div>
 
	</div>
 
</footer>
	
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/clipboard.min.js"></script>
<script src="assets/js/responsiveslides.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.4.3/plyr.min.js"></script>
<script src="assets/js/bulma.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>