<?php
	$router = new AltoRouter();
	$router->setBasePath(BASE_PATH);

	$router->map( 'GET', '/404', '404.php', 'imlost');

	$router->map( 'GET', '/kitchensink', 'layouts/kitchensink.php', 'kitchensink'); 
	$router->map( 'GET', '/tasks', 'layouts/tasks.php', 'tasks');
	
	$router->map( 'GET', '/about', 'layouts/about.php', 'about'); 
	$router->map( 'GET', '/pro', 'layouts/pro.php', 'pro'); 

	$router->map( 'GET', '/products', 'layouts/products.php', 'products'); 
	$router->map( 'GET', '/products/[:slug]', 'layouts/products-detail.php', 'products-detail');

	$router->map( 'GET', '/gallery', 'layouts/gallery.php', 'gallery');
	$router->map( 'GET', '/gallery/[:slug]', 'layouts/gallery-detail.php', 'gallery-detail');

	$router->map( 'GET', '/result', 'layouts/result.php', 'result'); 
	$router->map( 'GET', '/splashscreen', 'layouts/splashscreen.php', 'splashscreen');
	$router->map( 'GET', '/slick', 'layouts/slick.php', 'slick');
	$router->map( 'GET', '/inout', 'layouts/inout.php', 'inout');

	$router->map( 'GET', '/contact', 'layouts/contact.php', 'contact');
//	$router->map( 'GET', '/thanks', 'layouts/thank.php', 'thank');
	$router->map( 'GET', '/[:slug]', 'layouts/page.php', 'single-page');
	
	$home = $router->map( 'GET', '/', 'layouts/home.php', 'home'); //map home
	
	$match = $router->match();
?>