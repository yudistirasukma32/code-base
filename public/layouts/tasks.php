<?php
	$slug = 'kitchensink';
	$section = $slug;
?>

<?php require_once 'includes/header.php'; ?>

<section class="section">
	<div class="container">
		<div class="columns">
			<div class="column">
				<h1 class="title is-1">Tasks</h1>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">		
		<div class="columns">
			<div class="column">
				<div class="content">
					<h2 class="title is-3">Basic</h2>
					<ol>
						<li>Clone repository</li>
						<li>Composer Install</li>
						<li>Update Font</li>
						<li>Create custom class on scss (typograph/tweaks)</li>
						<li>Change color theme</li>
						<li>Create page about and contact (with form)</li>
						<li>Create content type product</li>
						<li>Create page products and product detail</li>
						<li>Push to repository git</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">		
		<div class="columns">
			<div class="column">
				<div class="content">
					<h2 class="title is-3">Intermediate</h2>
					<ol>
						<li>Create Search Query, see <a href="https://www.contentful.com/developers/docs/android/tutorials/advanced-filtering-and-searching/" target="_blank">Here</a></li>
						<li>Create Page Result for query results</li>
						<li>Add animation (smooth hover, parallax effect, fade in on scroll, etc)</li>
						<li>Add carousel image banner (use <a href="http://responsiveslides.com/" target="_blank">responsiveslides</a> / <a href="https://kenwheeler.github.io/slick/" target="_blank">slick</a>)</li>
						<li>Submit Email via Contact Form with sendgrid</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>

<?php require_once 'includes/footer.php'; ?>