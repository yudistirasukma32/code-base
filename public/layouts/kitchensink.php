<?php
	$slug = 'kitchensink';
	$section = $slug;
?>

<?php require_once 'includes/header.php'; ?>

<section class="section">
	<div class="container">
		<div class="columns">
			<div class="column">
				<h1 class="title is-1">Kitchensink</h1>
				<br/>
				<p class="subtitle">See all documentation @ <a href="https://bulma.io/documentation" target="_blank">Bulma</a></p>
			</div>
		</div>
	</div>
</section>

<?php require_once 'includes/footer.php'; ?>