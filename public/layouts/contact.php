<?php
	$slug = 'contact';
	$section = $slug;

	$query = new \Contentful\Delivery\Query;
	$query->setContentType('page')
				->where('fields.slug', $slug);
	$entries = $client->getEntries($query);

	if ($entries->getTotal() < 1) {
		_404();
	}
	$entry = $entries[0];
	setMetaTags($entry);
?>

<?php require_once 'includes/header.php'; ?>
<div class="bcontact">
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <h1 class="title is-1"><?= $entry->getTitle(); ?></h1>
        </div>
      </div>
    </div>
  </section>

  
    <section class="section">
      <div class="container">
        <div class="columns">
          <div class="column is-half-tablet is-offset-3-tablet">
            <form>
              <div class="field is-horizontal">
                <div class="field-label is-normal">
                  <label class="label">Name</label>
                </div>
                <div class="field-body">
                  <div class="field">
                    <p class="control is-expanded has-icons-left">
                      <input class="input" type="text" placeholder="Name">
                      <span class="icon is-small is-left">
                        <i class="fas fa-user"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>

              <div class="field is-horizontal">
                <div class="field-label is-normal">
                  <label class="label">Email</label>
                </div>
                <div class="field-body">
                  <div class="field">
                    <p class="control">
                      <input class="input is-success" type="email" placeholder="ex : novita@gmail.com" value="">
                      <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                      </span>
                      <span class="icon is-small is-right">
                        <i class="fas fa-check"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>

              <div class="field is-horizontal">
                <div class="field-label is-normal">
                  <label class="label">Subject</label>
                </div>
                <div class="field-body">
                  <div class="field">
                    <p class="control">
                      <input class="input is-success" type="subject" placeholder="Subject">
                      <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                      </span>
                      <span class="icon is-small is-right">
                        <i class="fas fa-check"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>

              <div class="field is-horizontal">
                <div class="field-label is-normal">
                  <label class="label">Question</label>
                </div>
                <div class="field-body">
                  <div class="field">
                    <div class="control">
                      <textarea class="textarea" placeholder="Explain how we can help you"></textarea>
                    </div>
                  </div>
                </div>
              </div>

              <div class="field is-horizontal">
                <div class="field-label">
                  <!-- Left empty for spacing -->
                </div>
                <div class="field-body">
                  <div class="field">
                    <div class="control">
                      <button class="button is-primary">
                        Send message
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php require_once 'includes/footer.php'; ?>